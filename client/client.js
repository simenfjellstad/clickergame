angular.module('app', []);
angular.module('app').controller('ClientController', function($scope, $interval, $http){
	$scope.socket = io();
	$scope.users = {};
	$scope.current_tick = 0;
	$scope.upgrades = {};
	$scope.auto = {};

	$scope.socket.on('sync', function(data){
		$scope.users = data.users;
		$scope.current_tick = data.current_tick;
		$scope.me = $scope.users[$scope.socket.id]
	})

	$scope.socket.on('upgrade-list', function(upgrades){
		$scope.upgrades = upgrades;
	})

	$scope.upgrade = function(id){
		$scope.socket.emit('upgrade', id);
	}

	$interval(function(){
		$scope.current_tick++;
		Object.keys($scope.users).map(function(userid, index) {
		    var user = $scope.users[userid];
		    var cps = user.cps;
		    Object.keys($scope.upgrades).map(function(upgradeid, index) {
		    	cps += $scope.upgrades[upgradeid].cps * ($scope.users[userid].upgrades[upgradeid] ? $scope.users[userid].upgrades[upgradeid] : 0);
		    	if($scope.auto[upgradeid]) $scope.upgrade(upgradeid);
		    });
		    user.coins += cps;
		});
	},1000)

	$scope.pow = function(x,n){ return Math.pow(x,n) }
	$scope.equate = function(base, ratio, level){
		return Math.round((base*ratio * Math.pow(ratio, level-1) - base) + base);
	}
	$scope.equateCost = function(id, upgrade, user){
		return $scope.equate(
			upgrade.base_cost, 
			upgrade.increase_rate,
			user.upgrades[id] ? user.upgrades[id] : 0
		)
	}

});