var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use('/node_modules', express.static('node_modules'));
app.use('/', express.static('client'));

var users = {};
var current_tick = 0;

var upgrades = {
	intern:{
		display_name:'Interns',
		base_cost: 100,
		increase_ratio:1.2,
		cps:1
	},
	apprentice:{
		display_name:'Apprentices',
		base_cost: 500,
		increase_ratio:1.3,
		cps:6
	},
	juniordev:{
		display_name:'Junior developers',
		base_cost: 1000,
		increase_ratio:1.35,
		cps:20
	},
	manager:{
		display_name:'Managers',
		base_cost: 12500,
		increase_ratio:1.4,
		cps:45
	},
	seniordev:{
		display_name:'Senior Developers',
		base_cost: 150000,
		increase_ratio:1.45,
		cps:100
	},
	freelancer:{
		display_name:'Freelancers',
		base_cost: 400000,
		increase_ratio:1.3,
		cps:1000
	},
	externalcompany:{
		display_name:'External Companies',
		base_cost: 1000000,
		increase_ratio:1.4,
		cps:5000
	}
}

app.get('/', function(req, res){
  res.sendfile('client/index.html');
});

io.on('connection', function(socket){
	socket.join('players'); //Join the room "players"
	users[socket.id] = {
		coins:100, 
		cps:1, 
		upgrades:{},
		joined_at:current_tick}
	
	socket.emit('upgrade-list', upgrades)
	socket.emit('sync', {users:getUsers(), tick:current_tick});

	socket.on('upgrade', function(id){
		if(!upgrades[id]) return;
		var level = users[socket.id].upgrades[id] ? users[socket.id].upgrades[id] : 0;
		var cost = calculateCost(
			upgrades[id].base_cost, 
			upgrades[id].increase_ratio,
			level
		)
		if(users[socket.id].coins >= cost){
			users[socket.id].coins-=cost;
			if(users[socket.id].upgrades[id] == undefined)
				users[socket.id].upgrades[id] = 0;
			users[socket.id].upgrades[id]++;
		}

		io.to('players').emit('sync', {users:getUsers(), tick:current_tick})
	})

  	socket.on('disconnect', function(){
		if(users[socket.id]) delete users[socket.id];
		io.to('players').emit('sync', {users:users, tick:current_tick})
  	});
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});

setInterval(function(){tick()}, 1000);

function getUsers(){
	Object.keys(users).map(function(userid, index) {
	    var user = users[userid];
	    var cps = user.cps;
	    Object.keys(upgrades).map(function(upgradeid, index) {
	    	cps += (upgrades[upgradeid].cps * (users[userid].upgrades[upgradeid] ? users[userid].upgrades[upgradeid] : 0));
	    });
	    users[userid].truecps = cps;
	});
	return users;
}

function tick(){
	current_tick++;
	Object.keys(users).map(function(userid, index) {
	    var user = users[userid];
	    var cps = user.cps;
	    Object.keys(upgrades).map(function(upgradeid, index) {
	    	cps += (upgrades[upgradeid].cps * (users[userid].upgrades[upgradeid] ? users[userid].upgrades[upgradeid] : 0));
	    });
	    user.coins += cps;
	});

	if(current_tick % 30 == 0) 
		io.to('players').emit('sync', {users:getUsers(), tick:current_tick})
}

function calculateCost(base, ratio, level){
	return Math.round((base*ratio * Math.pow(ratio, level-1) - base) + base);
}

